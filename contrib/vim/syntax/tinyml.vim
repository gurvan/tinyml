" Vim syntax file
" Language:    tinyml
" Author:      Gurvan <gurvan@debauss.art>

if exists("b:current_syntax")
  finish
endif
let b:current_syntax = "tml"

syn clear

syn keyword tmlType     int bool str chr uint
syn keyword tmlKeyword  let in fun type match end with pub use
syn keyword tmlOperator mod not + - * / < >
syn keyword tmlConst    true false
syn match   tmlComment  "#.*$"
syn match   tmlNumber   "\<[0-9]\+\>"

hi def link tmlType     Type
hi def link tmlKeyword  Keyword
hi def link tmlOperator Operator
hi def link tmlConst    Constant
hi def link tmlComment  Comment
hi def link tmlNumber   Number
