" Vim filetype plugin
" Language:    tinyml
" Author:      Gurvan <gurvan@debauss.art>

if exists('b:did_ftplugin')
  finish
endif
let b:did_ftplugin = 1

setlocal expandtab
setlocal tabstop=4
setlocal shiftwidth=4
setlocal softtabstop=0
setlocal textwidth=80
setlocal commentstring=#\ %s
