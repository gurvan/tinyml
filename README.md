# 🤏 tinyml

Tiny purely functional programming language.

## ⚖️  License

Distributed under the [Mozilla Public License Version 2.0](./LICENSE).
