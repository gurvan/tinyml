# Contributing

## Coding style

* Wrap lines at 80 characters (When reasonable to do so)
* Use two spaces instead of tabulation
* Remove any trailing white space

### Modules

* Only use open at the beginning of a file
* Order them first by decreasing length and then alphabetically

## Naming

| Name  | Description                   |
|-------|-------------------------------|
| b     | binary operator               |
| e     | expression                    |
| es    | expression list               |
| ea    | expression array              |
| ed    | expression definition         |
| eds   | expression definition list    |
| m     | matching element              |
| mp    | matching pattern              |
| ms    | matching element list         |
| p     | program                       |
| td    | type definition               |
| tds   | type definition list          |
| u     | unary operator                |
| v     | value                         |
| vs    | value list                    |
| va    | value array                   |
| x     | identifier                    |
| xs    | identifier list               |
