# TODO

## Syntax

* Allow terminating comma in arrays:
```tml
let struct = (
  fst : 5,
  snd : 5, # <-- here
)
```
* Allow sharing pattern to the same expression (true | false -> 0)
* Idea: Using `'` could existentially quantify both in types and in match.
```tml
let id(m n : int)
  match n
  |  m -> 0
  | 'x -> x
  end
```
  would mean that if n = m, then 0 else n. Note: Very ugly
* Making a function that immediately match on it's parameter:
```tml
let id = fun
  | 'x -> x
  end
```
* `with` keyword to copy struct content:
```tml
let a = (fst snd : 5) in
(with a, lst : 5)         # = (fst snd lst : 5)
```

## Compiler

* Make a simplification pass:
  - transform array from (id * 'a) list to simple lists
  - unfold all TVar
  - remove EFld
  - remove type definitions from the program
* Possible targets: QBE / LLVM

## New features

* Add new types: uint, chr (char), str (strings)
* External function calls
* Unit testing, formal specifications
* Struct and type concatenation:
  - `int * int = (int, int) = (int) * (int)`
  - `(fst : 3) * (snd : 3) = (fst snd : 3)`
* Sub-typing: allow `syracuse(5)`
* Inductive types. Note: Just like functions, all type definitions are recursive
  by default, and shadowing isn't allowed.
* Polymorphism
* Mutually recursive local functions
* Type classes

## Documentation

* Explain general language design choice
