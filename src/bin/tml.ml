(** tinyml ********************************************************************)
(*  Copyright Gurvan Debaussart (https://debauss.art)                         *)
(*  This file is distributed under the Mozilla Public License Version 2.0     *)
(******************************************************************************)

open Common.Pp
open Format
open Tinyml
open Sys

let version = "0.1.0-dev"

let help () =
  eprintf "tinyml %s\n" version;
  eprintf "usage:\n";
  eprintf "    tml <command> <arguments>\n";
  eprintf "commands:\n";
  eprintf "    exec\n";
  eprintf "    help\n";
  eprintf "    init\n";
  eprintf "    make\n";
  eprintf "    test\n"

let make _file = failwith "todo"
let test _file = failwith "todo"
let init () = failwith "todo"

let exec file =
  try
    eprintf "%a interpreting %s@." info () file;
    let prog  = Main.from_file file     in
    let value = Interpreter.i_prog prog in
    printf "%s@." (Pp.value value)
  with
  | Failure e        -> eprintf "failure: %s@." e
  | Tinyml.Error.E e -> eprintf "%a %a@." error () Tinyml.Error.pp e
  | Common.Error.E e -> eprintf "%a %a@." error () Common.Error.pp e

let () =
  if Array.length argv < 2 then help () else
  let input = Array.sub argv 2 (Array.length argv - 2) in
  match argv.(1) with
  | "exec" -> Array.iter exec input
  | "init" -> init ()
  | "make" -> Array.iter make input
  | "test" -> Array.iter test input
  | _      -> help ()
