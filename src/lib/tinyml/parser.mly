(** tinyml ********************************************************************)
(*  Copyright Gurvan Debaussart (https://debauss.art)                         *)
(*  This file is distributed under the Mozilla Public License Version 2.0     *)
(******************************************************************************)

%{
  open Common.Utils
  open Common.Ast
  open Ast
%}

(* Keywords *)
%token LET FUN IN TYPE MATCH END SET

(* Special characters *)
%token LPAR RPAR LBRA RBRA HOLE BAR COLON COMMA DOT RARROW EOF

(* Types *)
%token BOOL INT

(* Operations *)
%token ADD SUB MUL DIV MOD
%token EQU NEQ LT GT LEQ GEQ
%token NOT AND OR

(* Values *)
%token <int>    CINT
%token <bool>   CBOOL
%token <string> ID

%start program
%type <Ast.prog> program

(* Priorities --------------------------------------------------------------- *)

%left OR AND
%left LT GT LEQ GEQ EQU NEQ
%left MOD
%left ADD SUB
%left MUL DIV
%left NOT
%left LPAR DOT

%%

id:
| x=ID { IName x }
| HOLE { IAnon   }

(* Structures --------------------------------------------------------------- *)

arr_elt(elt):
| e=elt              { [IAnon, e]                  }
| xs=id+ COLON e=elt { List.map (fun x -> x, e) xs }

arr(elt):
| LPAR ea=separated_list(COMMA, arr_elt(elt)) RPAR { mk_arr (List.flatten ea) }

(* Types  ------------------------------------------------------------------- *)

typ:
| INT                           { TInt         }
| BOOL                          { TBol         }
| FUN ta=arr(typ) RARROW t=typ  { TFun (ta, t) }
| ta=arr(typ)                   { TArr ta      }
| x=ID                          { TVar x       }
| LBRA t=typ RBRA               { t            }

type_def:
| TYPE x=ID SET t=typ { x, t }

(* Expressions ---------------------------------------------------------------*)

%inline bop:
| ADD { BAdd } | SUB { BSub } | MUL { BMul } | DIV { BDiv } | MOD { BMod }
| LT  { BLt  } | GT  { BGt  } | LEQ { BLeq } | GEQ { BGeq }
| EQU { BEqu } | NEQ { BNeq }
| AND { BAnd } | OR  { BOr  }

%inline uop:
| ADD { UAdd } | SUB { UNeg }
| NOT { UNot }

value:
| n=CINT  { VInt n }
| b=CBOOL { VBol b }

expr_base:
| v=value                          { EVal v           }
| x=ID                             { EVar x           }
| ea=arr(expr)                     { EArr ea          }
| e=expr_base DOT x=ID             { EFld (e,  x)     }
| e=expr_base DOT n=CINT           { ENth (e,  n)     }
| e=expr_base ea=arr(expr)         { EApp (e, ea)     }
| u=uop e=expr_base                { EUop (u, e)      }
| e1=expr_base b=bop e2=expr_base  { EBop (b, e1, e2) }
| MATCH e=expr ms=mch_elt+ END     { EMch (e, ms)     }
| LBRA e=expr RBRA                 { e                }

expr:
| e=expr_base                   { e            }
| FUN ta=arr(typ) RARROW e=expr { EFun (ta, e) }
| ed=expr_def IN e=expr         { ELet (ed, e) }

mch_elt:
| BAR e1=mch_pat RARROW e2=expr { e1, e2 }

mch_pat:
| e=expr { MExpr e }
| HOLE   { MHole   }

expr_def:
| LET x=id COLON t=typ SET e=expr
    { { name = x; anot = t; body = e } }
| LET x=id ta=arr(typ) COLON t=typ SET e=expr
    { { name = x; anot = TFun (ta, t); body = EFun (ta, e) } }

(* Program ------------------------------------------------------------------ *)

program:
| tds=type_def* eds=expr_def* EOF
    { { typs = strmap_of_list tds; defs = eds } }
