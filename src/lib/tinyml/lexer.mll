(** tinyml ********************************************************************)
(*  Copyright Gurvan Debaussart (https://debauss.art)                         *)
(*  This file is distributed under the Mozilla Public License Version 2.0     *)
(******************************************************************************)

{
  open Lexing
  open Parser
  open Error
}

let alpha = ['a'-'z']
let num   = ['0'-'9']+
let id    = alpha (alpha | '_' | num)*
let space = [' ' '\n']+

rule token = parse
  (* Operations ------------------------------------------------------------- *)
  | "!"      { NOT                                                             }
  | "+"      { ADD                                                             }
  | "-"      { SUB                                                             }
  | "*"      { MUL                                                             }
  | "/"      { DIV                                                             }
  | "%"      { MOD                                                             }
  | "!="     { NEQ                                                             }
  | "=="     { EQU                                                             }
  | "<"      { LT                                                              }
  | "<="     { LEQ                                                             }
  | ">"      { GT                                                              }
  | ">="     { GEQ                                                             }
  | "&&"     { AND                                                             }
  | "||"     { OR                                                              }
  | "mod"    { MOD                                                             }
  (* Special characters ----------------------------------------------------- *)
  | "("      { LPAR                                                            }
  | ")"      { RPAR                                                            }
  | "{"      { LBRA                                                            }
  | "}"      { RBRA                                                            }
  | ":"      { COLON                                                           }
  | ","      { COMMA                                                           }
  | "."      { DOT                                                             }
  | "->"     { RARROW                                                          }
  | "|"      { BAR                                                             }
  | "="      { SET                                                             }
  | "_"      { HOLE                                                            }
  | "#"      { comnt lexbuf                                                    }
  | space    { token lexbuf                                                    }
  | eof      { EOF                                                             }
  (* Keywords --------------------------------------------------------------- *)
  | "type"   { TYPE                                                            }
  | "fun"    { FUN                                                             }
  | "let"    { LET                                                             }
  | "in"     { IN                                                              }
  | "match"  { MATCH                                                           }
  | "end"    { END                                                             }
  | "int"    { INT                                                             }
  | "bool"   { BOOL                                                            }
  (* Values ----------------------------------------------------------------- *)
  | "true"   { CBOOL true                                                      }
  | "false"  { CBOOL false                                                     }
  | num as n { CINT (int_of_string n)                                          }
  | id  as x { ID x                                                            }
  (* Unknown ---------------------------------------------------------------- *)
  | _ { raise (E (Lexing_error (lexeme lexbuf)))                               }

and comnt = parse
  | '\n' { token lexbuf }
  | _    { comnt lexbuf }
