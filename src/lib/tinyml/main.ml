(** tinyml ********************************************************************)
(*  Copyright Gurvan Debaussart (https://debauss.art)                         *)
(*  This file is distributed under the Mozilla Public License Version 2.0     *)
(******************************************************************************)

open Error

let from_file (f : string) =
  if not (Sys.file_exists f) then raise (E (Input_not_found f));
  if (Sys.is_directory f) then raise (E (Input_dir f));
  let c   = open_in f             in
  let lb  = Lexing.from_channel c in
  let p =
    try  Parser.program Lexer.token lb
    with Parser.Error -> raise (E (Parsing_error (Lexing.lexeme lb)))
  in
  ignore (Typer.t_prog p);
  p
