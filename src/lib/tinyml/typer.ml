(** tinyml ********************************************************************)
(*  Copyright Gurvan Debaussart (https://debauss.art)                         *)
(*  This file is distributed under the Mozilla Public License Version 2.0     *)
(******************************************************************************)

open Common.Utils
open Common.Ast
open Error
open Utils
open Ast

let t_prog p =

  let rec assert_eq t1 t2 =
    if not (t_eq p t1 t2) then raise (E (Bad_type (t1, t2)))

  and t_value = function
    | VInt _            -> TInt
    | VBol _            -> TBol
    | VArr  va          -> TArr (arr_map t_value va)
    | VFun (ta, e, env) -> t_vfun ta e env

  and t_vfun ta e env =
    let env = Env.map env t_value in
    let env = Env.add_arr env ta  in
    TFun (ta, t_expr env e)

  and t_expr env = function
    | EVal v -> t_value v
    | EVar x -> Env.get env x
    (* Basics *)
    | EUop (op, e)      -> t_uop env op e
    | EBop (op, e1, e2) -> t_bop env op e1 e2
    | EMch (e, ms)      -> t_mch env e ms
    | ELet (d, e)       -> t_let env d e
    (* Functions *)
    | EFun (ta, e)   -> TFun (ta, t_expr (Env.add_arr env ta) e)
    | EApp (e, ea)   -> t_app env e ea
    (* Arrays *)
    | EArr ea     -> TArr (arr_map (t_expr env) ea)
    | EFld (e, f) -> t_fld env e f
    | ENth (e, n) -> t_nth env e n

  and t_uop env op e =
    let te, ret =
      match op with
      | UAdd -> TInt, TInt
      | UNeg -> TInt, TInt
      | UNot -> TBol, TBol
    in
    assert_eq te (t_expr env e);
    ret

  and t_bop env op e1 e2 =
    let te, ret =
      match op with
      | BAdd | BSub | BMul | BMod | BDiv  -> TInt, TInt
      | BLt  | BLeq | BGt  | BGeq         -> TInt, TBol
      | BAnd | BOr                        -> TBol, TBol
      | BEqu | BNeq                       -> TInt, TBol
    in
    assert_eq te (t_expr env e1);
    assert_eq te (t_expr env e2);
    ret

  and t_mch env e ms =
    let t1   = t_expr env e in
    let t_mp = function
      | MExpr e1 -> t_expr env e1
      | MHole    -> t1
    in
    match ms with
    | (mp, e2) :: ms' ->
        assert_eq t1 (t_mp mp);
        let t2 = t_expr env e2 in
        List.iter (fun (mp, e2) ->
          assert_eq t1 (t_mp mp);
          assert_eq t2 (t_expr env e2)
        ) ms';
        t2
    | [] -> assert false (* unreacheable, would be parser error *)

  and t_let env d e =
    let t = t_expr env d.body in
    assert_eq t d.anot;
    t_expr (Env.add env d.name t) e

  and t_app env e ea =
    let t = t_expr env e in
    let ta, t =
      match t_simplify p t with
      | TFun(ta, t) -> TArr ta, t
      | t           -> raise (E (Type_not_fun (e, t)))
    in
    assert_eq ta (TArr (arr_map (t_expr env) ea));
    t

  and t_nth env e n =
    let t = t_expr env e in
    match t_simplify p t with
    | TArr a -> Utils.get_nth a n
    | t      -> raise (E (Type_not_array (e, t)))

  and t_fld env e f =
    let t = t_expr env e in
    match t_simplify p t with
    | TArr a -> Utils.get_fld a f
    | t      -> raise (E (Type_not_array (e, t)))
  in

  let t_def env (d : def) =
    ignore (t_expr env d.body)
  in

  let env =
    List.fold_left (fun acc d -> Env.add acc d.name d.anot) Env.empty p.defs
  in
  List.iter (t_def env) p.defs
