(** tinyml ********************************************************************)
(*  Copyright Gurvan Debaussart (https://debauss.art)                         *)
(*  This file is distributed under the Mozilla Public License Version 2.0     *)
(******************************************************************************)

open Common.Pp
open Format
open Ast

let rec typ = function
  | TInt          -> "int"
  | TBol          -> "bool"
  | TFun (ta, t)  -> sprintf "fun%s -> %s" (arr typ ta) (typ t)
  | TArr ta       -> arr typ ta
  | TVar x        -> x

let rec value = function
  | VInt n          -> string_of_int n
  | VBol b          -> string_of_bool b
  | VFun (ta, e, _) -> sprintf "fun%s -> %s" (arr typ ta) (expr e)
  | VArr va         -> arr value va

and expr = function
  | EVar x -> x
  | EVal v -> value v
  (* Basics *)
  | EUop (u, e)      -> sprintf "(%s %s)" (uop u) (expr e)
  | EBop (b, e1, e2) -> sprintf "(@[%s %s %s)@]" (expr e1) (bop b) (expr e2)
  | ELet (d, e)      -> sprintf "@[(%s in@ %s)@]" (def d) (expr e)
  | EMch (e, ms)     -> sprintf "@[match %s\n%s\nend@]" (expr e) (mch_elt ms)
  (* Functions *)
  | EFun (ta, e)   -> sprintf "fun%s -> %s" (arr typ ta) (expr e)
  | EApp (e, ea)   -> sprintf "%s%s" (expr e) (arr expr ea)
  (* Arrays *)
  | EArr ea     -> arr expr ea
  | EFld (e, x) -> sprintf "(%s).%s" (expr e) x
  | ENth (e, n) -> sprintf "(%s).%d" (expr e) n

and mch_elt ms =
  List.map (fun (mp, e2) -> sprintf "| %s -> %s" (mch_pat mp) (expr e2)) ms
  |> String.concat "\n"

and mch_pat = function
  | MExpr e -> expr e
  | MHole   -> "_"

and def d =
  sprintf "@[let %s : %s = %s@]" (id d.name) (typ d.anot) (expr d.body)
