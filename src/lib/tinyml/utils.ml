(** tinyml ********************************************************************)
(*  Copyright Gurvan Debaussart (https://debauss.art)                         *)
(*  This file is distributed under the Mozilla Public License Version 2.0     *)
(******************************************************************************)

open Common.Utils
open Common.Ast
open Error
open Ast

(* Typing ------------------------------------------------------------------- *)

let rec t_simplify p = function
  | TVar x ->
    begin match StringMap.find_opt x p.typs with
    | Some t -> t
    | None   -> raise (E (Unknown_type x))
    end
  | TArr ta -> TArr (arr_map (t_simplify p) ta)
  | t       -> t

let t_eq p t1 t2 =
  (* TODO: Handle the more subtle case of arrays *)
  let t1 = t_simplify p t1 in
  let t2 = t_simplify p t2 in
  t1 = t2

(* Runtime ------------------------------------------------------------------ *)

let val_eq v1 v2 =
  (* TODO: Handle the more subtle case of arrays *)
  match v1, v2 with
  | VInt n1, VInt n2 -> n1 = n2
  | VBol b1, VBol b2 -> b1 = b2
  | _                -> false

(* Arrays ------------------------------------------------------------------- *)

let get_nth (a : 'a arr) (n : int) : 'a =
  (if n < 0 then raise (E (Bad_nth n)));
  let anon, _ = a in
  match List.nth_opt anon n with
  | Some t -> t
  | None   -> raise (E (Bad_nth n))

let get_fld (a : 'a arr) (f : string) : 'a =
  let _, name = a in
  match StringMap.find_opt f name with
  | Some t -> t
  | None   -> raise (E (Bad_fld f))
