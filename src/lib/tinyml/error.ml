(** tinyml ********************************************************************)
(*  Copyright Gurvan Debaussart (https://debauss.art)                         *)
(*  This file is distributed under the Mozilla Public License Version 2.0     *)
(******************************************************************************)

open Format

type t =
  (* Frontend --------------------------------------------------------------- *)
  | Input_not_found of string
  | Input_dir       of string
  | Parsing_error   of string
  | Lexing_error    of string
  (* Typing ----------------------------------------------------------------- *)
                    (* expected  found *)
  | Bad_type        of Ast.typ * Ast.typ
  | Unknown_type    of string
  | Bad_nth         of int
  | Bad_fld         of string
  | Type_not_array  of Ast.expr * Ast.typ
  | Type_not_fun    of Ast.expr * Ast.typ
  (* Interpreter ------------------------------------------------------------ *)
  | Typer_error

exception E of t

let pp fmt = function
  | Input_not_found f ->
      fprintf fmt "Input '%s' does not exists" f
  | Input_dir f ->
      fprintf fmt "Input '%s' is a directory" f
  | Parsing_error s ->
      fprintf fmt "Found unexpected '%s'" (String.escaped s)
  | Lexing_error s ->
      fprintf fmt "Unknown character '%s'" (String.escaped s)
  | Bad_type (t1, t2)->
      fprintf fmt "Expected type '%s' but found '%s'" (Pp.typ t1) (Pp.typ t2)
  | Unknown_type x ->
      fprintf fmt "Unknown type '%s'" x
  | Bad_nth n ->
      fprintf fmt "Bad nth '%d'" n
  | Bad_fld f ->
      fprintf fmt "Bad field '%s'" f
  | Type_not_array (e, t) ->
      fprintf fmt "Expression '%s' has type '%s', which is not an array"
        (Pp.expr e) (Pp.typ t)
  | Type_not_fun (e, t) ->
      fprintf fmt "Expression '%s' has type '%s', which is not a function"
        (Pp.expr e) (Pp.typ t)
  | Typer_error ->
      fprintf fmt "Typer error. Please open an issue."
