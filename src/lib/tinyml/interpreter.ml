(** tinyml ********************************************************************)
(*  Copyright Gurvan Debaussart (https://debauss.art)                         *)
(*  This file is distributed under the Mozilla Public License Version 2.0     *)
(******************************************************************************)

open Common.Utils
open Common.Ast
open Error
open Utils
open Ast

let rec i_expr env = function
  | EVal v            -> v
  | EVar x            -> Env.get env x
  (* Basics *)
  | EUop (op, e)      -> i_uop env op e
  | EBop (op, e1, e2) -> i_bop env op e1 e2
  | EMch (e, ms)      -> i_mch env e ms
  | ELet (d, e)       -> i_let env d e
  (* Functions *)
  | EFun (ta, e)      -> VFun (ta, e, env)
  | EApp (e1, e2)     -> i_app env e1 e2
  (* Arrays *)
  | EArr ea           -> VArr (arr_map (i_expr env) ea)
  | EFld (e, f)       -> i_fld env e f
  | ENth (e, n)       -> i_nth env e n

and i_uop env op e =
  match op, i_expr env e with
  | UAdd, VInt n -> VInt n
  | UNeg, VInt n -> VInt (-n)
  | UNot, VBol b -> VBol (not b)
  | _ -> assert false

and i_bop env op e1 e2 =
  match op, i_expr env e1, i_expr env e2 with
  | BAdd, VInt n1, VInt n2 -> VInt (n1 + n2)
  | BMul, VInt n1, VInt n2 -> VInt (n1 * n2)
  | BSub, VInt n1, VInt n2 -> VInt (n1 - n2)
  | BDiv, VInt n1, VInt n2 -> VInt (n1 / n2)
  | BMod, VInt n1, VInt n2 -> VInt (n1 mod n2)
  | BLt,  VInt n1, VInt n2 -> VBol (n1 < n2)
  | BLeq, VInt n1, VInt n2 -> VBol (n1 <= n2)
  | BGt,  VInt n1, VInt n2 -> VBol (n1 > n2)
  | BGeq, VInt n1, VInt n2 -> VBol (n1 >= n2)
  | BAnd, VBol b1, VBol b2 -> VBol (b1 && b2)
  | BOr,  VBol b1, VBol b2 -> VBol (b1 || b2)
  | BEqu,      v1,      v2 -> VBol (Utils.val_eq v1 v2)
  | BNeq,      v1,      v2 -> VBol (not (Utils.val_eq v1 v2))
  | _ -> raise (E (Typer_error))

and i_mch env e ms =
  let v = i_expr env e in
  let mch_ok (mp, _) =
    match mp with
    | MExpr e' -> Utils.val_eq v (i_expr env e')
    | MHole    -> true
    in
  match List.find_opt mch_ok ms with
  | Some (_, e) -> i_expr env e
  | None        -> raise (E (Typer_error))

and i_app env e ea =
  let v  = i_expr env e in
  let va = arr_map (i_expr env) ea in
  (* TODO: This is wrong. We cannot simply add_arr here because anonymous value
in the struct may be associated to a type in tarr. We need a v_cast and t_cast
function *)
  match v with
  | VFun (_, e, env) -> i_expr (Env.add_arr env va) e
  | _                -> raise (E (Typer_error))

and i_let env d e =
  let env = Env.add env d.name (i_expr env d.body) in
  i_expr env e

and i_fld env e f =
  match i_expr env e with
  | VArr va -> get_fld va f
  | _       -> raise (E (Typer_error))

and i_nth env e n =
  match i_expr env e with
  | VArr va -> get_nth va n
  | _       -> raise (E (Typer_error))

let i_prog (p : prog) : value =
  let env =
    List.fold_left (fun acc d -> Env.add acc d.name d.body) Env.empty p.defs
  in
  let main = Env.get env "main" in
  (* TODO: Create an environment with globals *)
  i_expr Env.empty (EApp (main, arr_empty))
