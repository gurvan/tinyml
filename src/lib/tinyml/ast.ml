(** tinyml ********************************************************************)
(*  Copyright Gurvan Debaussart (https://debauss.art)                         *)
(*  This file is distributed under the Mozilla Public License Version 2.0     *)
(******************************************************************************)

open Common.Utils
open Common.Ast

type typ =
  | TInt
  | TBol
  | TFun of typ arr * typ
  | TArr of typ arr
  | TVar of string

type value =
  | VInt of int
  | VBol of bool
  | VFun of tfun
  | VArr of value arr

and expr =
  | EVal of value
  | EVar of string
  (* Basics *)
  | EUop of uop * expr
  | EBop of bop * expr * expr
  | ELet of def * expr
  | EMch of expr * (mch * expr) list
  (* Functions *)
  | EFun of typ arr * expr
  | EApp of expr * expr arr
  (* Arrays *)
  | EArr of expr arr      (* Creation of an array      *)
  | EFld of expr * string (* Access a named field      *)
  | ENth of expr * int    (* Access to the n-th field  *)

and tfun = typ arr * expr * value Env.t

and mch =
  | MExpr of expr
  | MHole

and def = {
  name : id;
  anot : typ;
  body : expr;
}

type prog = {
  typs : typ StringMap.t;
  defs : def list;
}
