(** tinyml ********************************************************************)
(*  Copyright Gurvan Debaussart (https://debauss.art)                         *)
(*  This file is distributed under the Mozilla Public License Version 2.0     *)
(******************************************************************************)

module StringMap = Map.Make(String)

type id =
  | IName of string
  | IAnon

type 'a arr = 'a list * 'a StringMap.t

type uop = UAdd | UNeg | UNot
type bop = BAdd | BSub | BMul | BDiv | BMod
         | BEqu | BNeq | BLt  | BLeq | BGt | BGeq
         | BAnd | BOr
