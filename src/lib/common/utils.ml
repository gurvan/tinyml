(** tinyml ********************************************************************)
(*  Copyright Gurvan Debaussart (https://debauss.art)                         *)
(*  This file is distributed under the Mozilla Public License Version 2.0     *)
(******************************************************************************)

open Error
open Ast

(* Identifiers -------------------------------------------------------------- *)

let id_eq i1 i2 =
  match i1, i2 with
  | IName n1, IName n2 -> n1 = n2
  | IAnon,    IAnon    -> true
  | _                  -> false

let uv_id = function
  | IName x -> x
  | IAnon   -> assert false

let is_anon = function
  | IName _ -> false
  | IAnon   -> true

(* String maps -------------------------------------------------------------- *)

let strmap_of_list (l : (string * 'a) list) : 'a StringMap.t =
  let add acc (k, v) =
    match StringMap.find_opt k acc with
    | None   -> StringMap.add k v acc
    | Some _ -> raise (E (Shadowing k))
  in
  List.fold_left add StringMap.empty l

(* Arrays ------------------------------------------------------------------- *)

let mk_arr l =
  let anon, named = List.partition (fun (x, _) -> is_anon x) l in
  let named = List.map (fun (x, a) -> uv_id x, a) named in
  List.map snd anon, strmap_of_list named

let arr_map (f : 'a -> 'b) (a : 'a arr) : 'b arr =
  let anon, name = a in
  List.map f anon, StringMap.map f name

let arr_empty : 'a arr =
  [], StringMap.empty

(* Environments ------------------------------------------------------------- *)

module Env = struct
  type 'a t = 'a StringMap.t

  let empty : 'a t = StringMap.empty

  let add (env : 'a t) (x : id) (t : 'a) =
    match x with
    | IName x -> StringMap.add x t env
    | IAnon   -> env

  let del (env : 'a t) (x : id) =
    match x with
    | IName x -> StringMap.remove x env
    | IAnon   -> env

  let get (env : 'a t) (x : string) =
    try StringMap.find x env
    with Not_found -> raise (E (Unknown x))

  let get_opt (env : 'a t) (x : string) =
    StringMap.find_opt x env

  let map (env : 'a t) (f : 'a -> 'b) =
    StringMap.map f env

  let fold = StringMap.fold

  let to_string (env : 'a t) (pp : 'a -> string) =
    let open Format in
    StringMap.fold (fun v t acc -> sprintf "%s%s : %s\n" acc v (pp t)) env "\n"

  let add_arr (env : 'a t) (arr : 'a arr) =
    let _, named = arr in
    StringMap.union (fun _ _ a -> Some a) env named

  let of_strmap (s : 'a StringMap.t) : 'a t =
    s
end
