(** tinyml ********************************************************************)
(*  Copyright Gurvan Debaussart (https://debauss.art)                         *)
(*  This file is distributed under the Mozilla Public License Version 2.0     *)
(******************************************************************************)

open Format

type t =
  | Unknown   of string
  | Shadowing of string

exception E of t

let pp fmt = function
  | Shadowing s -> fprintf fmt "Multiple declaration with the same name '%s'" s
  | Unknown   x -> fprintf fmt "Unknown var '%s'" x
