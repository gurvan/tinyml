(** tinyml ********************************************************************)
(*  Copyright Gurvan Debaussart (https://debauss.art)                         *)
(*  This file is distributed under the Mozilla Public License Version 2.0     *)
(******************************************************************************)

open Format
open Ast

let error fmt () =
  fprintf fmt "error:"

let info fmt () =
  fprintf fmt "info:"

let warning fmt () =
  fprintf fmt "warning:"

let id = function
  | IName n -> n
  | IAnon   -> "_"

let arr (str : 'a -> string) (a : 'a arr) =
  let anon, name = a in
  List.map str anon
  |> String.concat ", "
  |> StringMap.fold (fun x a acc -> sprintf "%s : %s, %s" x (str a) acc) name
  |> sprintf "(@[%s)@]"

let uop = function
  | UAdd -> "+"
  | UNeg -> "-"
  | UNot -> "!"

let bop = function
  | BAdd -> "+"
  | BSub -> "-"
  | BMul -> "*"
  | BDiv -> "/"
  | BMod -> "%"
  | BEqu -> "="
  | BNeq -> "!="
  | BLt  -> "<"
  | BLeq -> "<="
  | BGt  -> ">"
  | BGeq -> ">="
  | BAnd -> "&&"
  | BOr  -> "||"
